# account_service

the purpose of the project is to provide 2 http endpoints to 1- create bank accounts for a given customerId,
and 2- view the customer information with the list of created accounts and the corresponding transactions which are the result of the first endpoint.

## Getting started

To be able to use the 1st endpoint we need to have the customer in the system to create an account for, hence there is a class called 
`CustomerInitialFill` to populate 3 customerIds which can be used for using the endpoints.

to be able to use the 1st endpoint run:

`curl --location --request POST 'http://localhost:8080/account' \
--header 'Content-Type: application/json' \
--data-raw '{
"amount": 100,
"customerId": "359e5094-c1d7-11ed-afa1-0242ac120002",
"currency": "EUR"
}'`

the 2nd endpoint:

`curl --location --request GET 'http://localhost:8080/transactions?customerId=359e5094-c1d7-11ed-afa1-0242ac120002'`


## technologies
The project is written in Java 17 and utilizes the Vert.x framework, which is a lightweight event-driven and non-blocking solution capable of handling high concurrency with minimal kernel threads. This design approach ensures a robust and scalable application. Furthermore, the project implements the hexagonal architecture for its overall structure. At present, in-memory data structures are used to store data, but this design is easily extensible to support any type of database in the future.

## Project status
Currently, the customer and transaction are inside of this service, which are going to be removed and work as separate microservices along with the account-service 
to provide more scalability.

`ApplicationVerticle` has the responsibility of creating an HttpServer and handling the incoming requests. that sets up a web server to handle incoming HTTP requests. When the server receives a POST request to the /account endpoint, it sends a message with the request data to an event bus. When it receives a response from the event bus, it sends the response back to the client who made the original request.
Similarly, when the server receives a GET request to the /transactions endpoint, it extracts the customer ID from the request and sends a message with the ID to the event bus. Again, when it receives a response from the event bus, it sends the response back to the client who made the original request.
There are two other verticles that handle the messages received by the event bus. These verticles are designed to process the messages sent from the ApplicationVerticle class, which serves as a kind of central hub or "dispatcher" for incoming messages.

## Security(TBD)
To enhance the security of the system, we can use two tools: Kubernetes ServiceAccounts and Istio. Kubernetes ServiceAccounts authenticate and authorize access to Kubernetes resources, while Istio provides features such as traffic management, security, observability, and policy enforcement by injecting sidecars into pods. By using these tools, we can ensure that the APIs are protected by robust authorization, authentication, and encryption mechanisms.