package com.capgemini.accountserviceapp;

import com.capgemini.accountserviceapp.adapters.account.model.CreateAccountRequest;
import com.capgemini.accountserviceapp.adapters.account.model.Currency;
import com.capgemini.accountserviceapp.adapters.account.model.CustomerAccountsRequest;
import com.capgemini.accountserviceapp.adapters.account.model.CustomerAccountsResponse;
import com.capgemini.accountserviceapp.adapters.exception.CustomerNotFoundException;
import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsRequest;
import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsResponse;
import com.capgemini.accountserviceapp.adapters.transaction.model.CreateAccountTransactionResponse;
import com.capgemini.accountserviceapp.adapters.transaction.model.CreateTransactionRequest;
import com.capgemini.accountserviceapp.common.GenericCodec;
import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.AccountStatus;
import com.capgemini.accountserviceapp.domain.model.Amount;
import com.capgemini.accountserviceapp.domain.model.BankAccount;
import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import com.capgemini.accountserviceapp.domain.model.Transaction;
import com.capgemini.accountserviceapp.domain.model.TransactionIdentifier;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class TestDataGenerator {

    public static CreateAccountRequest createAccountRequest(UUID customerUuid, long amount) {
        return CreateAccountRequest.builder()
                .customerId(customerUuid.toString())
                .amount(amount)
                .currency(Currency.USD).build();
    }

    public static CreateAccountRequest createAccountRequest(String customerUuid, long amount) {
        return CreateAccountRequest.builder()
                .customerId(customerUuid)
                .amount(amount)
                .currency(Currency.EUR).build();
    }

    public static Customer createCustomer(UUID customerId){
        return new Customer(CustomerId.of(customerId), "Alice", "Smith", "testAddress", "alice@example.com", "061234567");
    }

    public static BankAccount createBankAccount(String accountId, Customer customer, Instant createdTime, AccountStatus status) {
        return new BankAccount(AccountIdentifier.of(accountId), customer, Amount.of(10,
                com.capgemini.accountserviceapp.domain.model.Currency.EUR),
                createdTime, createdTime.plusSeconds(60), createdTime, null, status);
    }

    public static List<Transaction> createTransactions(String accountNumber, Instant transactionDate) {
        return List.of(new Transaction(
                        TransactionIdentifier.of(randomString()),
                        transactionDate,
                        Amount.of(10, com.capgemini.accountserviceapp.domain.model.Currency.EUR),
                        "description",
                        null,
                        AccountIdentifier.of(accountNumber)
                )
        );
    }

    public static Instant fixedDate() {
        return Instant.now();
    }

    public static String randomString() {
        return RandomStringUtils.randomNumeric(16);
    }

    public static UUID randomUUID() { return UUID.randomUUID();}

    public static String createJsonAccountRequest(String customerId, long amount)  {
        return new JsonObject()
                .put("amount", amount)
                .put("customerId", customerId)
                .put("currency", "EUR")
                .toString();
    }

    public static void registerCodec(Vertx vertx) {
        vertx.eventBus().registerDefaultCodec(CustomerAccountsRequest.class,
                new GenericCodec<>(CustomerAccountsRequest.class));
        vertx.eventBus().registerDefaultCodec(AccountIdentifier.class,
                new GenericCodec<>(AccountIdentifier.class));
        vertx.eventBus().registerDefaultCodec(CreateAccountRequest.class,
                new GenericCodec<>(CreateAccountRequest.class));
        vertx.eventBus().registerDefaultCodec(AccountTransactionsRequest.class,
                new GenericCodec<>(AccountTransactionsRequest.class));
        vertx.eventBus().registerDefaultCodec(AccountTransactionsResponse.class,
                new GenericCodec<>(AccountTransactionsResponse.class));
        vertx.eventBus().registerDefaultCodec(CustomerAccountsResponse.class,
                new GenericCodec<>(CustomerAccountsResponse.class));
        vertx.eventBus().registerDefaultCodec(CreateTransactionRequest.class,
                new GenericCodec<>(CreateTransactionRequest.class));
        vertx.eventBus().registerDefaultCodec(CreateAccountTransactionResponse.class,
                new GenericCodec<>(CreateAccountTransactionResponse.class));
        vertx.eventBus().registerDefaultCodec(CustomerNotFoundException.class,
                new GenericCodec<>(CustomerNotFoundException.class));
    }
}
