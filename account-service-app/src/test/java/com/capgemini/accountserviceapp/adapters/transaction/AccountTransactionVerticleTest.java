package com.capgemini.accountserviceapp.adapters.transaction;


import com.capgemini.accountserviceapp.TestDataGenerator;
import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsRequest;
import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsResponse;
import com.capgemini.accountserviceapp.adapters.transaction.model.CreateTransactionRequest;
import com.capgemini.accountserviceapp.adapters.transaction.model.Currency;
import com.capgemini.accountserviceapp.common.JsonConverter;
import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.Transaction;
import com.capgemini.accountserviceapp.domain.ports.AccountTransactions;
import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith({
        VertxExtension.class,
})
public class AccountTransactionVerticleTest {
    private final AccountTransactions accountTransactions = mock(AccountTransactions.class);
    private final JsonConverter jsonConverter = new JsonConverter();

    @BeforeEach
    void setUp(Vertx vertx) {
        AccountTransactionsVerticle accountTransactionsVerticle = new AccountTransactionsVerticle(jsonConverter, accountTransactions);
        TestDataGenerator.registerCodec(vertx);
        vertx.deployVerticle(accountTransactionsVerticle);
    }

    @Test
    public void testTransactionList(Vertx vertx, VertxTestContext testContext) {
        String accountNumber = TestDataGenerator.randomString();
        Instant trDate = TestDataGenerator.fixedDate();
        List<Transaction> transactions = TestDataGenerator.createTransactions(accountNumber, trDate);

        when(accountTransactions.with(AccountIdentifier.of(accountNumber))).thenReturn(transactions);
        List<String> accountNrs = List.of(accountNumber);

        vertx.eventBus().request("com.capgemini.accountservice.transactions", jsonConverter.convertFromObjectToJson(AccountTransactionsRequest.builder().accountNumbers(accountNrs).build()),
                response -> testContext.verify(() -> {
                    AccountTransactionsResponse transactionsResponse = jsonConverter.convertFromJsonToObject(response.result().body().toString(),AccountTransactionsResponse.class);
                    assertThat(transactionsResponse.getAccountTransactions().get(accountNumber).size()).isEqualTo(1);
                    assertThat(transactionsResponse.getAccountTransactions().get(accountNumber)).isInstanceOf(List.class)
                            .as("transactions should be a list of Transaction objects")
                            .allSatisfy(tr -> assertThat(tr).isInstanceOf(com.capgemini.accountserviceapp.adapters.transaction.model.Transaction.class));
                    assertThat(transactionsResponse.getAccountTransactions().get(accountNumber).stream()
                            .anyMatch(transaction -> transaction.getDestinationAccount().equals(accountNumber))
                    ).isEqualTo(true);
                    testContext.completeNow();
                })
        );
    }

    @Test
    public void testCreateTransactionSuccess(Vertx vertx, VertxTestContext testContext) {
        String accountNumber = TestDataGenerator.randomString();

        CreateTransactionRequest createTransactionRequest = new CreateTransactionRequest(accountNumber, 100, Currency.EUR);
        doNothing().when(accountTransactions).save(any(AccountIdentifier.class), any(Transaction.class));
        vertx.eventBus().request("com.capgemini.accountservice.create.transaction", jsonConverter.convertFromObjectToJson(createTransactionRequest),
                response -> testContext.verify(() -> {
                    assertThat(response.failed()).isFalse();
                    testContext.completeNow();
                }));
    }

    @Test
    public void testCreateTransactionFailed(Vertx vertx, VertxTestContext testContext) {
        String accountNumber = TestDataGenerator.randomString();

        CreateTransactionRequest createTransactionRequest = new CreateTransactionRequest(accountNumber, 100, Currency.EUR);
        doThrow(RuntimeException.class).when(accountTransactions).save(any(AccountIdentifier.class), any(Transaction.class));
        vertx.eventBus().request("com.capgemini.accountservice.create.transaction", jsonConverter.convertFromObjectToJson(createTransactionRequest),
                response -> testContext.verify(() -> {
                    assertThat(response.failed()).isTrue();
                    testContext.completeNow();
                }));
    }

}