package com.capgemini.accountserviceapp.adapters.incomingapi;

import com.capgemini.accountserviceapp.TestDataGenerator;
import com.capgemini.accountserviceapp.adapters.account.AccountVerticle;
import com.capgemini.accountserviceapp.adapters.account.model.CustomerAccountsResponse;
import com.capgemini.accountserviceapp.adapters.customer.CustomerService;
import com.capgemini.accountserviceapp.adapters.transaction.AccountTransactionsVerticle;
import com.capgemini.accountserviceapp.adapters.transaction.model.CreateAccountTransactionResponse;
import com.capgemini.accountserviceapp.common.JsonConverter;
import com.capgemini.accountserviceapp.domain.model.AccountStatus;
import com.capgemini.accountserviceapp.domain.model.BankAccount;
import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import com.capgemini.accountserviceapp.domain.ports.AccountTransactions;
import com.capgemini.accountserviceapp.domain.ports.BankAccounts;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith({
        VertxExtension.class,
})
class ApplicationVerticleTest {

    private final BankAccounts bankAccounts = mock(BankAccounts.class);
    private final CustomerService customerService = mock(CustomerService.class);
    private final AccountTransactions accountTransactions = mock(AccountTransactions.class);

    private final JsonConverter jsonConverter = new JsonConverter();

    @BeforeEach
    void setUp(Vertx vertx, VertxTestContext testContext) {
        AccountVerticle accountVerticle = new AccountVerticle(bankAccounts, customerService, jsonConverter);
        AccountTransactionsVerticle transactionsVerticle = new AccountTransactionsVerticle(jsonConverter, accountTransactions);
        vertx.deployVerticle(new ApplicationVerticle(jsonConverter), testContext.succeedingThenComplete());
        TestDataGenerator.registerCodec(vertx);
        vertx.deployVerticle(accountVerticle);
        vertx.deployVerticle(transactionsVerticle);
    }

    @Test
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    public void testCreateSuccessfulAccountAndTransaction(Vertx vertx, VertxTestContext testContext) {
        UUID customerId = TestDataGenerator.randomUUID();
        String createAccountRequest = TestDataGenerator.createJsonAccountRequest(customerId.toString(), 100);

        Customer customer = TestDataGenerator.createCustomer(customerId);

        when(customerService.getCustomer(any())).thenReturn(customer);

        HttpClient client = vertx.createHttpClient();
        client.request(HttpMethod.POST, 8080, "localhost", "/account")
                .compose(req -> req.send(createAccountRequest).compose(HttpClientResponse::body))
                .onComplete(
                        testContext.succeeding(buffer -> {
                            CreateAccountTransactionResponse accountTransactionResponse = jsonConverter.convertFromJsonToObject(buffer.toString(), CreateAccountTransactionResponse.class);
                            assertThat(accountTransactionResponse).isNotNull();
                            assertThat(accountTransactionResponse.getAccountNumber()).isNotNull();
                            assertThat(accountTransactionResponse.getTransactionId()).isNotNull();
                            testContext.completeNow();
                        })
                );
    }

    @Test
    public void testGetCustomerAccountTransactions(Vertx vertx, VertxTestContext testContext) {
        UUID customerId = TestDataGenerator.randomUUID();
        String accountNumber = TestDataGenerator.randomString();
        Customer customer = TestDataGenerator.createCustomer(customerId);
        BankAccount bankAccount = TestDataGenerator.createBankAccount(accountNumber, customer, Instant.now(), AccountStatus.OPEN);
        when(bankAccounts.with((CustomerId) any())).thenReturn(List.of(bankAccount));
        when(customerService.getCustomer(any())).thenReturn(customer);

        HttpClient client = vertx.createHttpClient();
        client.request(HttpMethod.GET, 8080, "localhost", "/transactions?customerId="+customerId)
                .compose(req -> req.send().compose(HttpClientResponse::body))
                .onComplete(
                        testContext.succeeding(buffer -> {
                            CustomerAccountsResponse accountTransactionsResponse = jsonConverter.convertFromJsonToObject(buffer.toString(), CustomerAccountsResponse.class);
                            assertThat(accountTransactionsResponse).isNotNull();
                            assertThat(accountTransactionsResponse.getCustomerSurname()).isEqualTo(customer.surname());
                            assertThat(accountTransactionsResponse.getCustomerName()).isEqualTo(customer.name());
                            assertThat(accountTransactionsResponse.getBalanceInCent()).isEqualTo(bankAccount.balance().amountInCents());
                            assertThat(accountTransactionsResponse.getAccountTransactions().getAccountTransactions().size()).isEqualTo(1);
                            testContext.completeNow();
                        })
                );

    }
}
