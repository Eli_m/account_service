package com.capgemini.accountserviceapp.domain.vertx;

import com.capgemini.accountserviceapp.TestDataGenerator;
import com.capgemini.accountserviceapp.adapters.account.AccountVerticle;
import com.capgemini.accountserviceapp.adapters.account.model.CreateAccountRequest;
import com.capgemini.accountserviceapp.adapters.account.model.CustomerAccountsResponse;
import com.capgemini.accountserviceapp.adapters.customer.CustomerService;
import com.capgemini.accountserviceapp.adapters.transaction.AccountTransactionsVerticle;
import com.capgemini.accountserviceapp.common.JsonConverter;
import com.capgemini.accountserviceapp.domain.model.AccountStatus;
import com.capgemini.accountserviceapp.domain.model.BankAccount;
import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import com.capgemini.accountserviceapp.domain.model.Transaction;
import com.capgemini.accountserviceapp.domain.ports.AccountTransactions;
import com.capgemini.accountserviceapp.domain.ports.BankAccounts;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.rxjava3.core.http.HttpClient;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(VertxExtension.class)
class AccountVerticleTest {

    private final BankAccounts bankAccounts = mock(BankAccounts.class);
    private final AccountTransactions accountTransactions = mock(AccountTransactions.class);
    private final CustomerService customerService = mock(CustomerService.class);
    private final JsonConverter jsonConverter = new JsonConverter();

    @BeforeEach
    public void setUp(Vertx vertx) {
        AccountVerticle accountVerticle = new AccountVerticle(bankAccounts, customerService, jsonConverter);
        AccountTransactionsVerticle accountTransactionsVerticle = new AccountTransactionsVerticle(jsonConverter, accountTransactions);
        TestDataGenerator.registerCodec(vertx);
        vertx.deployVerticle(accountVerticle);
        vertx.deployVerticle(accountTransactionsVerticle);
    }


    @Test
    @DisplayName("should receive a request to create an account for an existing customerId with amount greater than zero")
    void testReceiveCreateWithAmountAndCustomerId(Vertx vertx, VertxTestContext testContext) {
        UUID customerId = TestDataGenerator.randomUUID();
        String createAccountRequest = TestDataGenerator.createJsonAccountRequest(customerId.toString(), 100L);
        doNothing().when(bankAccounts).save(any());
        Customer customer = TestDataGenerator.createCustomer(customerId);
        when(customerService.getCustomer(any())).thenReturn(customer);

        vertx.eventBus().send("com.capgemini.accountservice.account", createAccountRequest);

        ArgumentCaptor<BankAccount> bankAccountCaptor = ArgumentCaptor.forClass(BankAccount.class);
        verify(bankAccounts, timeout(5000).times(1)).save(bankAccountCaptor.capture());

        testContext.verify(() -> {
            assertThat(bankAccountCaptor.getValue().balance().amountInCents()).isEqualTo(10000L);
            assertThat(bankAccountCaptor.getValue().customer().customerId().id()).isEqualTo(customerId);
            testContext.completeNow();
        });
    }

    @Test
    @DisplayName("should create an account for an existing customerId with amount zero, without creating a transaction")
    void testCreateWithAmountAndCustomerIdNoTransaction(Vertx vertx, VertxTestContext testContext) {
        UUID customerId =TestDataGenerator.randomUUID();
        String createAccountRequest = TestDataGenerator.createJsonAccountRequest(customerId.toString(), 0L);

        doNothing().when(bankAccounts).save(any());
        Customer customer = TestDataGenerator.createCustomer(customerId);
        when(customerService.getCustomer(any())).thenReturn(customer);


        HttpClient httpClient = Mockito.mock(HttpClient.class);
        io.vertx.rxjava3.core.Vertx vertx2 = Mockito.mock(io.vertx.rxjava3.core.Vertx.class);
        when(vertx2.createHttpClient(any(HttpClientOptions.class))).thenReturn(httpClient);

        vertx.eventBus().send("com.capgemini.accountservice.account", createAccountRequest);

        ArgumentCaptor<BankAccount> bankAccountCaptor = ArgumentCaptor.forClass(BankAccount.class);
        verify(bankAccounts, timeout(5000).times(1)).save(bankAccountCaptor.capture());

        testContext.verify(() -> {
            assertThat(bankAccountCaptor.getValue().balance().amountInCents()).isEqualTo(0L);
            assertThat(bankAccountCaptor.getValue().customer().customerId().id()).isEqualTo(customerId);
            verifyNoInteractions(httpClient);

            testContext.completeNow();
        });
    }

    @Test
    @DisplayName("should not create account for a malformed ")
    void testCreateWithAmountAndInvalidCustomerId(Vertx vertx, VertxTestContext testContext) {
        String customerId = "malformed customerId";
        String createAccountRequest = TestDataGenerator.createJsonAccountRequest(customerId, 100L);

        doNothing().when(bankAccounts).save(any());
        when(customerService.getCustomer(any())).thenReturn(null);

        HttpClient httpClient = Mockito.mock(HttpClient.class);
        io.vertx.rxjava3.core.Vertx vertx2 = Mockito.mock(io.vertx.rxjava3.core.Vertx.class);
        when(vertx2.createHttpClient(any(HttpClientOptions.class))).thenReturn(httpClient);

        vertx.eventBus().send("com.capgemini.accountservice.account", createAccountRequest);

        testContext.verify(() -> {
            verifyNoInteractions(bankAccounts);
            testContext.completeNow();
        });
    }

    @Test
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    @DisplayName("should not create an account for an unknown customerId with any amount")
    void testReceiveACreateWithAmountAndUnknownCustomerId(Vertx vertx, VertxTestContext testContext) {
        UUID customerId = TestDataGenerator.randomUUID();
        CreateAccountRequest createAccountRequest = TestDataGenerator.createAccountRequest(customerId, 100L);

        doNothing().when(bankAccounts).save(any());
        when(customerService.getCustomer(any())).thenReturn(null);

        vertx.eventBus().send("com.capgemini.accountservice.account", createAccountRequest);

        testContext.verify(() -> {
            verifyNoInteractions(bankAccounts);
            testContext.completeNow();
        });
    }

    @Test
    @DisplayName("should return a customer account response")
    void testCustomerAccountTransactions(Vertx vertx, VertxTestContext testContext) {
        UUID customerId = TestDataGenerator.randomUUID();
        String accountNumber = RandomStringUtils.randomNumeric(10);
        Instant createdTime = Instant.now();
        List<Transaction> transactions = List.of();

        Customer customer = TestDataGenerator.createCustomer(customerId);
        BankAccount bankAccount = TestDataGenerator.createBankAccount(accountNumber, customer, createdTime, AccountStatus.OPEN);

        when(customerService.getCustomer(any())).thenReturn(customer);
        when(bankAccounts.with((CustomerId) any())).thenReturn(List.of(bankAccount));
        when(accountTransactions.with(bankAccount.accountIdentifier())).thenReturn(transactions);

        vertx.eventBus().request("com.capgemini.accountservice.customer.account.transactions", customerId.toString(),
                response -> testContext.verify(() -> {
                    CustomerAccountsResponse customerAccountsResponse = jsonConverter.convertFromJsonToObject(response.result().body().toString(),CustomerAccountsResponse.class);
                    assertThat(customerAccountsResponse.getCustomerName()).isEqualTo(customer.name());
                    assertThat(customerAccountsResponse.getCustomerSurname()).isEqualTo(customer.surname());
                    assertThat(customerAccountsResponse.getBalanceInCent()).isEqualTo(bankAccount.balance().amountInCents());
                    assertThat(customerAccountsResponse.getAccountTransactions().getAccountTransactions().size()).isEqualTo(1);
                    testContext.completeNow();
                }));
    }


}