package com.capgemini.accountserviceapp.It;

import com.capgemini.accountserviceapp.TestDataGenerator;
import com.capgemini.accountserviceapp.adapters.account.AccountVerticle;
import com.capgemini.accountserviceapp.adapters.customer.CustomerService;
import com.capgemini.accountserviceapp.adapters.incomingapi.ApplicationVerticle;
import com.capgemini.accountserviceapp.adapters.transaction.AccountTransactionsVerticle;
import com.capgemini.accountserviceapp.adapters.transaction.model.CreateAccountTransactionResponse;
import com.capgemini.accountserviceapp.common.JsonConverter;
import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.ports.BankAccounts;
import com.capgemini.accountserviceapp.infrastructure.AccountTransactionsInMemory;
import com.capgemini.accountserviceapp.infrastructure.BankAccountsInMemory;
import com.capgemini.accountserviceapp.infrastructure.CustomerInMemory;
import com.capgemini.accountserviceapp.infrastructure.repository.AccountTransactionsInMemoryRepository;
import com.capgemini.accountserviceapp.infrastructure.repository.AccountsInMemoryRepository;
import com.capgemini.accountserviceapp.infrastructure.repository.CustomersInMemoryRepository;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith({
        VertxExtension.class,
})
class CreateBankAccountIT {

    private final BankAccounts bankAccounts = new BankAccountsInMemory(new AccountsInMemoryRepository());
    private final CustomerService customerService = new CustomerService(new CustomerInMemory(new CustomersInMemoryRepository()));
    private final AccountTransactionsInMemory accountTransactions = new AccountTransactionsInMemory(new AccountTransactionsInMemoryRepository());

    private final JsonConverter jsonConverter = new JsonConverter();


    @BeforeEach
    void setUp(Vertx vertx, VertxTestContext testContext) {
        AccountVerticle accountVerticle = new AccountVerticle(bankAccounts, customerService, jsonConverter);
        AccountTransactionsVerticle transactionsVerticle = new AccountTransactionsVerticle(jsonConverter, accountTransactions);
        vertx.deployVerticle(new ApplicationVerticle(jsonConverter), testContext.succeedingThenComplete());
        TestDataGenerator.registerCodec(vertx);
        vertx.deployVerticle(accountVerticle);
        vertx.deployVerticle(transactionsVerticle);
    }

    @Test
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    public void testCreateSuccessfulAccountAndTransaction(Vertx vertx, VertxTestContext testContext) {
        UUID customerId = TestDataGenerator.randomUUID();
        String createAccountRequest = TestDataGenerator.createJsonAccountRequest(customerId.toString(), 100);

        Customer customer = TestDataGenerator.createCustomer(customerId);
        customerService.saveCustomer(customer);

        HttpClient client = vertx.createHttpClient();
        client.request(HttpMethod.POST, 8080, "localhost", "/account")
                .compose(req -> req.send(createAccountRequest).compose(HttpClientResponse::body))
                .onComplete(
                        testContext.succeeding(buffer -> {
                            CreateAccountTransactionResponse accountTransactionResponse = jsonConverter.convertFromJsonToObject(buffer.toString(), CreateAccountTransactionResponse.class);
                            assertThat(accountTransactionResponse).isNotNull();
                            assertThat(accountTransactionResponse.getAccountNumber()).isNotNull();
                            assertThat(accountTransactionResponse.getTransactionId()).isNotNull();
                            testContext.completeNow();
                        })
                );

    }
}
