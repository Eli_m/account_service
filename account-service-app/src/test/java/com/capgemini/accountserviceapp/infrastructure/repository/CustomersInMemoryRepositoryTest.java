package com.capgemini.accountserviceapp.infrastructure.repository;

import com.capgemini.accountserviceapp.TestDataGenerator;
import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CustomersInMemoryRepositoryTest {

    @Test
    public void testAddAndGetCustomerById() {
        CustomersInMemoryRepository repository = new CustomersInMemoryRepository();
        UUID customerId = TestDataGenerator.randomUUID();
        Customer customer = TestDataGenerator.createCustomer(customerId);

        repository.addCustomer(customer);
        assertEquals(customer, repository.getCustomer(customer.customerId()));
        assertNull(repository.getCustomer(new CustomerId(TestDataGenerator.randomUUID())));
    }

}