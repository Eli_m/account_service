package com.capgemini.accountserviceapp.infrastructure.repository;

import com.capgemini.accountserviceapp.TestDataGenerator;
import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.Amount;
import com.capgemini.accountserviceapp.domain.model.Currency;
import com.capgemini.accountserviceapp.domain.model.Transaction;
import com.capgemini.accountserviceapp.domain.model.TransactionIdentifier;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountTransactionsInMemoryRepositoryTest {

    @Test
    void testSaveAndRetrieveTransactions() {
        AccountTransactionsInMemoryRepository repository = new AccountTransactionsInMemoryRepository();

        String transactionId = TestDataGenerator.randomString();
        AccountIdentifier accountIdentifier = AccountIdentifier.of(TestDataGenerator.randomString());

        Transaction transaction = new Transaction(TransactionIdentifier.of(transactionId),TestDataGenerator.fixedDate(),
                Amount.of(100, Currency.EUR),"testTransaction",null,accountIdentifier);

        repository.saveTransaction(accountIdentifier, transaction);

        assertEquals(1, repository.getTransactions(accountIdentifier).size());
        assertThat(repository.getTransactions(accountIdentifier).get(0).transactionIdentifier().id()).isEqualTo(transactionId);
        assertThat(repository.getTransactions(accountIdentifier).get(0).destinationAccountId()).isEqualTo(accountIdentifier);
        assertThat(repository.getTransactions(accountIdentifier).get(0).sourceAccountId()).isEqualTo(null);
    }

}