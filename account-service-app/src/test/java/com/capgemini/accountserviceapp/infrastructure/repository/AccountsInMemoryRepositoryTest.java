package com.capgemini.accountserviceapp.infrastructure.repository;

import com.capgemini.accountserviceapp.TestDataGenerator;
import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.AccountStatus;
import com.capgemini.accountserviceapp.domain.model.BankAccount;
import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class AccountsInMemoryRepositoryTest {

    @Test
    public void testSaveAndRetrieveAccountById() {
        AccountsInMemoryRepository repository = new AccountsInMemoryRepository();
        String accountNumber = TestDataGenerator.randomString();
        UUID customerId = TestDataGenerator.randomUUID();

        Customer customer = TestDataGenerator.createCustomer(customerId);

        BankAccount account = TestDataGenerator.createBankAccount(accountNumber,customer,TestDataGenerator.fixedDate(),AccountStatus.OPEN);
        repository.saveAccount(account);
        assertEquals(account, repository.getAccount(account.accountIdentifier()));
        assertNull(repository.getAccount(new AccountIdentifier("999")));
    }

    @Test
    public void testSaveAndRetrieveAccountByCustomerId() {
        AccountsInMemoryRepository repository = new AccountsInMemoryRepository();
        UUID customerId = TestDataGenerator.randomUUID();
        String accountNumber1 = TestDataGenerator.randomString();
        String accountNumber2 = TestDataGenerator.randomString();
        // Create two BankAccount objects with the same customer
        Customer customer = TestDataGenerator.createCustomer(customerId);

        BankAccount firstAccount = TestDataGenerator.createBankAccount(accountNumber1,customer, TestDataGenerator.fixedDate(),AccountStatus.CLOSE);
        BankAccount secondAccount = TestDataGenerator.createBankAccount(accountNumber2,customer,TestDataGenerator.fixedDate(),AccountStatus.OPEN);

        repository.saveAccount(firstAccount);
        repository.saveAccount(secondAccount);

        assertEquals(firstAccount, repository.getAccount(AccountIdentifier.of(accountNumber1)));
        assertNotEquals(secondAccount, repository.getAccount(customer.customerId()));

        assertThat(repository.getAccount(new CustomerId(TestDataGenerator.randomUUID())).size()).isEqualTo(0);
    }
}