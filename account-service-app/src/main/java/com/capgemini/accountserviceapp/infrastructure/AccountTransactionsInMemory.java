package com.capgemini.accountserviceapp.infrastructure;

import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.Transaction;
import com.capgemini.accountserviceapp.domain.ports.AccountTransactions;
import com.capgemini.accountserviceapp.infrastructure.repository.AccountTransactionsInMemoryRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountTransactionsInMemory implements AccountTransactions {

    private final AccountTransactionsInMemoryRepository accountTransactionsInMemoryRepository;

    public AccountTransactionsInMemory(AccountTransactionsInMemoryRepository accountTransactionsInMemoryRepository) {
        this.accountTransactionsInMemoryRepository = accountTransactionsInMemoryRepository;
    }

    @Override
    public List<Transaction> with(AccountIdentifier accountIdentifier) {
        return accountTransactionsInMemoryRepository.getTransactions(accountIdentifier);
    }

    @Override
    public void save(AccountIdentifier accountIdentifier, Transaction transaction) {
        accountTransactionsInMemoryRepository.saveTransaction(accountIdentifier,transaction);
    }
}
