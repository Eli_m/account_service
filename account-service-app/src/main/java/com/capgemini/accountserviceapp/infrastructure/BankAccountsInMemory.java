package com.capgemini.accountserviceapp.infrastructure;

import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.BankAccount;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import com.capgemini.accountserviceapp.domain.ports.BankAccounts;
import com.capgemini.accountserviceapp.infrastructure.repository.AccountsInMemoryRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BankAccountsInMemory implements BankAccounts {

    private final AccountsInMemoryRepository accountsInMemoryRepository;

    public BankAccountsInMemory(AccountsInMemoryRepository accountsInMemoryRepository) {
        this.accountsInMemoryRepository = accountsInMemoryRepository;
    }

    @Override
    public BankAccount with(AccountIdentifier accountIdentifier) {
        return accountsInMemoryRepository.getAccount(accountIdentifier);
    }

    @Override
    public void save(BankAccount bankAccount) {
       accountsInMemoryRepository.saveAccount(bankAccount);
    }

    @Override
    public List<BankAccount> with(CustomerId customerId) {
        return accountsInMemoryRepository.getAccount(customerId);
    }
}
