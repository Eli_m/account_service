package com.capgemini.accountserviceapp.infrastructure.repository;

import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class CustomersInMemoryRepository {
    private final Set<Customer> customers = new HashSet<>();

    public void addCustomer(Customer customer){
        customers.add(customer);
    }
    public Customer getCustomer(CustomerId customerId){
        return customers.stream().filter(customer -> customer.customerId().equals(customerId)).findFirst().orElse(null);
    }
}
