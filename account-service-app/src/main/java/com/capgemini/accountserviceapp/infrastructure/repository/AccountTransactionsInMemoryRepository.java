package com.capgemini.accountserviceapp.infrastructure.repository;

import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.Transaction;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AccountTransactionsInMemoryRepository {
    private final Map<AccountIdentifier, List<Transaction>> transactions = new HashMap<>();

    public void saveTransaction(AccountIdentifier accountIdentifier, Transaction transaction){
        transactions.computeIfAbsent(accountIdentifier, tr -> new ArrayList<>()).add(transaction);
    }
    public List<Transaction> getTransactions(AccountIdentifier accountIdentifier){
        return transactions.get(accountIdentifier);
    }

}
