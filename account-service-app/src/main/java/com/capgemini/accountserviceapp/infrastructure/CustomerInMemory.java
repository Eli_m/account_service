package com.capgemini.accountserviceapp.infrastructure;

import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import com.capgemini.accountserviceapp.domain.ports.Customers;
import com.capgemini.accountserviceapp.infrastructure.repository.CustomersInMemoryRepository;
import org.springframework.stereotype.Component;

@Component
public class CustomerInMemory implements Customers {

    private final CustomersInMemoryRepository customersInMemoryRepository;

    public CustomerInMemory(CustomersInMemoryRepository customersInMemoryRepository) {
        this.customersInMemoryRepository = customersInMemoryRepository;
    }

    @Override
    public Customer with(CustomerId customerId) {
        return customersInMemoryRepository.getCustomer(customerId);
    }

    @Override
    public void save(Customer customer) {
        customersInMemoryRepository.addCustomer(customer);
    }
}
