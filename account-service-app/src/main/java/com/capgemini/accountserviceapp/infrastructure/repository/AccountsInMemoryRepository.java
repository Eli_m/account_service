package com.capgemini.accountserviceapp.infrastructure.repository;

import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.BankAccount;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Component
public class AccountsInMemoryRepository {
    private final Set<BankAccount> bankAccounts = new HashSet<>();

    public void saveAccount(BankAccount bankAccount){
        bankAccounts.add(bankAccount);
    }
    public BankAccount getAccount(AccountIdentifier accountIdentifier){
        return bankAccounts.stream().filter(account -> account.accountIdentifier().equals(accountIdentifier)).findFirst().orElse(null);
    }

    public List<BankAccount> getAccount(CustomerId customerId){
        return bankAccounts.stream().filter(account -> account.customer().customerId().equals(customerId)).toList();
    }
}
