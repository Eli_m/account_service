package com.capgemini.accountserviceapp;

import com.capgemini.accountserviceapp.adapters.account.AccountVerticle;
import com.capgemini.accountserviceapp.adapters.incomingapi.ApplicationVerticle;
import com.capgemini.accountserviceapp.adapters.transaction.AccountTransactionsVerticle;
import io.vertx.rxjava3.core.Vertx;
import jakarta.annotation.PostConstruct;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountServiceAppApplication {

    private final ApplicationVerticle applicationVerticle;
    private final AccountVerticle accountVerticle;
    private final AccountTransactionsVerticle accountTransactionsVerticle;

    public AccountServiceAppApplication(ApplicationVerticle applicationVerticle, AccountVerticle accountVerticle, AccountTransactionsVerticle accountTransactionsVerticle) {
        this.applicationVerticle = applicationVerticle;
        this.accountVerticle = accountVerticle;
        this.accountTransactionsVerticle = accountTransactionsVerticle;
    }

    public static void main(String[] args) {
        SpringApplication.run(AccountServiceAppApplication.class, args);
    }

    @PostConstruct
    public void deployVerticles() {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(applicationVerticle);
        vertx.deployVerticle(accountVerticle);
        vertx.deployVerticle(accountTransactionsVerticle);
    }

}
