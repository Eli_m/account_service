package com.capgemini.accountserviceapp.domain.ports;

import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.Transaction;
import org.jmolecules.ddd.annotation.Repository;

import java.util.List;

@Repository
public interface AccountTransactions {
    List<Transaction> with(AccountIdentifier accountIdentifier);
    void save(AccountIdentifier accountIdentifier, Transaction transaction);
}
