package com.capgemini.accountserviceapp.domain.model;

import org.jmolecules.ddd.annotation.Entity;
import org.jmolecules.ddd.annotation.Identity;


@Entity
public record Customer(
  @Identity
  CustomerId customerId,
  String name,
  String surname,
  String address,
  String email,
  String phone
) {

}
