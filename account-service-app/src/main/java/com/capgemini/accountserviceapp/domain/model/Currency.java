package com.capgemini.accountserviceapp.domain.model;

import org.jmolecules.ddd.annotation.ValueObject;

@ValueObject
public enum Currency {
    EUR, GBP, USD
}
