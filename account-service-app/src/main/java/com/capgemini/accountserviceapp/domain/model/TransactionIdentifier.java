package com.capgemini.accountserviceapp.domain.model;

import org.apache.commons.lang3.RandomStringUtils;
import org.jmolecules.ddd.annotation.ValueObject;

@ValueObject
public record TransactionIdentifier(String id) {

    public static TransactionIdentifier of(){
        return new TransactionIdentifier(RandomStringUtils.randomNumeric(16));
    }
    public static TransactionIdentifier of(String id){
        return new TransactionIdentifier(id);
    }

}
