package com.capgemini.accountserviceapp.domain.model;

import org.apache.commons.lang3.RandomStringUtils;
import org.jmolecules.ddd.annotation.ValueObject;

@ValueObject
public record AccountIdentifier(String id) {

    public static AccountIdentifier of(){
        return new AccountIdentifier(RandomStringUtils.randomNumeric(10));
    }
    public static AccountIdentifier of(String id){
        return new AccountIdentifier(id);
    }

}
