package com.capgemini.accountserviceapp.domain.model;

import org.jmolecules.ddd.annotation.ValueObject;

import java.util.UUID;

@ValueObject
public record CustomerId(UUID id) {

    public static CustomerId of(UUID id){
        return new CustomerId(id);
    }

}
