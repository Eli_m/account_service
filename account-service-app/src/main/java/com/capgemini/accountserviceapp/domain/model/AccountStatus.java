package com.capgemini.accountserviceapp.domain.model;

import org.jmolecules.ddd.annotation.ValueObject;

@ValueObject
public enum AccountStatus {
    OPEN,
    CLOSE,
    BLOCKED
}
