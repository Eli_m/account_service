package com.capgemini.accountserviceapp.domain.model;

import org.jmolecules.ddd.annotation.Identity;

import java.time.Instant;

public record Transaction(@Identity
                          TransactionIdentifier transactionIdentifier,
                          Instant instant,
                          Amount amount,
                          String description,
                          AccountIdentifier sourceAccountId,
                          AccountIdentifier destinationAccountId) {

}
