package com.capgemini.accountserviceapp.domain.ports;

import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import org.jmolecules.ddd.annotation.Repository;

@Repository
public interface Customers {
    Customer with(CustomerId customerId);
    void save(Customer customer);
}
