package com.capgemini.accountserviceapp.domain.model;


import org.jmolecules.ddd.annotation.Entity;
import org.jmolecules.ddd.annotation.Identity;

import java.time.Instant;

@Entity
public record BankAccount(
        @Identity
        AccountIdentifier accountIdentifier,
        Customer customer,
        Amount balance,
        Instant createdAt,
        Instant lastUpdatedAt,
        Instant openedAt,
        Instant closedAt,
        AccountStatus status
) {

}


