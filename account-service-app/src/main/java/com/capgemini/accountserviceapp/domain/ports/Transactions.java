package com.capgemini.accountserviceapp.domain.ports;

import com.capgemini.accountserviceapp.domain.model.Transaction;
import com.capgemini.accountserviceapp.domain.model.TransactionIdentifier;
import org.jmolecules.ddd.annotation.Repository;

@Repository
public interface Transactions {
    Transaction with(TransactionIdentifier transactionIdentifier);
    void save(Transaction transaction);
}
