package com.capgemini.accountserviceapp.domain.model;

import org.jmolecules.ddd.annotation.ValueObject;

@ValueObject
public record Amount(long amountInCents, Currency currency) {

    private static final String currencyMessage = "amounts should have the same currency";

    public Amount plus(Amount other) {
        assert hasSameCurrency(other) : currencyMessage;

        return new Amount(amountInCents + other.amountInCents(), currency);
    }

    public static Amount of(double amount, Currency currency) {
        assert currency != null;

        return ofCents(Math.round(amount * 100), currency);
    }

    public static Amount ofCents(long amountInCents, Currency currency) {
        assert currency != null;

        return new Amount(amountInCents, currency);
    }

    public Amount minus(Amount other) {
        assert hasSameCurrency(other) : currencyMessage;
        assert amountInCents >= other.amountInCents() : "Cannot subtract more than the available amount";

        return new Amount(amountInCents - other.amountInCents, currency);
    }

    public boolean isZero(Amount amount){
        return amount.amountInCents == 0;
    }

    public boolean hasSameCurrency(Amount other) {
        return other.currency == currency;
    }

}
