package com.capgemini.accountserviceapp.domain.ports;

import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.BankAccount;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import org.jmolecules.ddd.annotation.Repository;

import java.util.List;

@Repository
public interface BankAccounts {
    BankAccount with(AccountIdentifier accountIdentifier);
    void save(BankAccount bankAccount);
    List<BankAccount> with(CustomerId customerId);

}
