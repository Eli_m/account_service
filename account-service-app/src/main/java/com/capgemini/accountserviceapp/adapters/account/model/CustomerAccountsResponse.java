package com.capgemini.accountserviceapp.adapters.account.model;

import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerAccountsResponse {
    private String customerName;
    private String customerSurname;
    private double balanceInCent;
    private AccountTransactionsResponse accountTransactions;
}
