package com.capgemini.accountserviceapp.adapters.customer;

import com.capgemini.accountserviceapp.adapters.account.UUIDHelper;
import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;


@Component
public class CustomerInitialFill {

    private final CustomerService customerService;

    public CustomerInitialFill(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostConstruct
    public void init() {
        String customerUuid1 = "359e5094-c1d7-11ed-afa1-0242ac120002";
        String customerUuid2 = "7918f798-c1d7-11ed-afa1-0242ac120002";
        String customerUuid3 = "82e7c11e-c1d7-11ed-afa1-0242ac120002";

        Customer customer1 = new Customer(CustomerId.of(UUIDHelper.fromString(customerUuid1)), "Alice", "Smith", "testAddress", "alice@example.com", "061234567");
        Customer customer2 = new Customer(CustomerId.of(UUIDHelper.fromString(customerUuid2)), "Elahe", "Mohaddes", "testAddress", "alice@example.com", "061234567");
        Customer customer3 = new Customer(CustomerId.of(UUIDHelper.fromString(customerUuid3)), "Kiana", "Jones", "testAddress", "alice@example.com", "061234567");

        customerService.saveCustomer(customer1);
        customerService.saveCustomer(customer2);
        customerService.saveCustomer(customer3);
    }

}
