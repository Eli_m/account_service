package com.capgemini.accountserviceapp.adapters.incomingapi;

import com.capgemini.accountserviceapp.common.JsonConverter;
import io.reactivex.rxjava3.disposables.Disposable;
import io.vertx.rxjava3.core.AbstractVerticle;
import io.vertx.rxjava3.core.http.HttpServer;
import io.vertx.rxjava3.ext.web.Router;
import io.vertx.rxjava3.ext.web.RoutingContext;
import io.vertx.rxjava3.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ApplicationVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final JsonConverter jsonConverter;

    public ApplicationVerticle(JsonConverter jsonConverter) {
        this.jsonConverter = jsonConverter;
    }

    @Override
    public void start() {
        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        // Define the routes
        router.post("/account").handler(this::handleAccountRequest);
        router.get("/transactions").handler(this::handleTransactionsRequest);

        final Disposable[] disposable = new Disposable[1];
        disposable[0] = server.requestHandler(router)
                .rxListen(8080)
                .doOnError(error -> {
                    logger.error("Failed to start server {}" , error.getMessage());
                    disposable[0].dispose();
                })
                .subscribe(httpServer -> logger.info("Server started on port {}" , httpServer.actualPort()));

    }

    private void handleAccountRequest(RoutingContext routingContext)  {
        vertx.eventBus()
                .rxRequest("com.capgemini.accountservice.account", routingContext.getBodyAsJson())
                .subscribe(result -> routingContext.response().setChunked(true).end(result.body().toString()),
                        error -> routingContext.response().setStatusCode(400).putHeader("content-type", "application/json; charset=utf-8").setStatusMessage(error.getMessage()).end());

    }

    private void handleTransactionsRequest(RoutingContext routingContext) {
        vertx.eventBus().rxRequest("com.capgemini.accountservice.customer.account.transactions",routingContext.request().getParam("customerId"))
                .subscribe(result -> routingContext.response().end(result.body().toString()),
                        error -> routingContext.response().setStatusCode(400).putHeader("content-type", "application/json; charset=utf-8").setStatusMessage(error.getMessage()).end());
    }

}
