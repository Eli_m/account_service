package com.capgemini.accountserviceapp.adapters.transaction;


import com.capgemini.accountserviceapp.domain.model.Transaction;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class TransactionConverter {

    public static com.capgemini.accountserviceapp.adapters.transaction.model.Transaction convertToTransaction(Transaction transaction) {
        return com.capgemini.accountserviceapp.adapters.transaction.model.Transaction.builder().transactionId(transaction.transactionIdentifier().id())
                .amount(transaction.amount().amountInCents()).description(transaction.description())
                .dateTime(LocalDateTime.ofInstant(transaction.instant(), ZoneId.of("Europe/Amsterdam")).toString())
                .sourceAccount(transaction.sourceAccountId() != null ? transaction.sourceAccountId().id() : null)
                .destinationAccount(transaction.destinationAccountId().id()).build();
    }
}
