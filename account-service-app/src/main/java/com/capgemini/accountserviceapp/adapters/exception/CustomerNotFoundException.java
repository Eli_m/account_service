package com.capgemini.accountserviceapp.adapters.exception;

import java.util.UUID;

public class CustomerNotFoundException extends RuntimeException{

    public CustomerNotFoundException(UUID uid) {
        this(uid, null);
    }

    public CustomerNotFoundException(UUID uid, Throwable cause) {
        super(String.format("Customer [id: %s] does not exist",  uid), cause);
    }

}
