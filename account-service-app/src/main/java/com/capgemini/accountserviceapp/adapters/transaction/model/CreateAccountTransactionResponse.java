package com.capgemini.accountserviceapp.adapters.transaction.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateAccountTransactionResponse {
    private String accountNumber;
    private String transactionId;
}
