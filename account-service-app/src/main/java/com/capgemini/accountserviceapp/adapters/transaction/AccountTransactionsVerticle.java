package com.capgemini.accountserviceapp.adapters.transaction;

import com.capgemini.accountserviceapp.adapters.exception.ErrorCode;
import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsRequest;
import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsResponse;
import com.capgemini.accountserviceapp.adapters.transaction.model.CreateAccountTransactionResponse;
import com.capgemini.accountserviceapp.adapters.transaction.model.CreateTransactionRequest;
import com.capgemini.accountserviceapp.common.JsonConverter;
import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.Amount;
import com.capgemini.accountserviceapp.domain.model.Currency;
import com.capgemini.accountserviceapp.domain.model.Transaction;
import com.capgemini.accountserviceapp.domain.model.TransactionIdentifier;
import com.capgemini.accountserviceapp.domain.ports.AccountTransactions;
import io.vertx.rxjava3.core.AbstractVerticle;
import io.vertx.rxjava3.core.eventbus.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//todo: this verticle should call transaction microservice
@Component
public class AccountTransactionsVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String CREATE_ACCOUNT_TRANSACTION_DESC = "create account transaction";
    private final JsonConverter jsonConverter;

    private final AccountTransactions accountTransactions;

    public AccountTransactionsVerticle(JsonConverter jsonConverter, AccountTransactions accountTransactions) {
        this.jsonConverter = jsonConverter;
        this.accountTransactions = accountTransactions;
    }

    @Override
    public void start() {
        handleCreateTransaction();
        handleTransactionList();
    }

    private void handleCreateTransaction() {
        // consumer for createTransaction request
        vertx.eventBus().consumer("com.capgemini.accountservice.create.transaction", message -> {
            CreateTransactionRequest createTransactionRequest = jsonConverter.convertFromJsonToObject(message.body().toString(),CreateTransactionRequest.class);
            handleCreateTransaction(createTransactionRequest, message);
        });
    }

    private void handleTransactionList() {
        // consumer for list of transactions request
        vertx.eventBus().consumer("com.capgemini.accountservice.transactions", message -> {
            AccountTransactionsRequest accountTransactionsRequest = jsonConverter.convertFromJsonToObject(message.body().toString(), AccountTransactionsRequest.class);

            Map<String, List<com.capgemini.accountserviceapp.adapters.transaction.model.Transaction>> accountTransactions = retrieveAndMapAccountTransactions(accountTransactionsRequest);
            message.reply(jsonConverter.convertFromObjectToJson(convertToAccountTransaction(accountTransactions)));
        });
    }

    private Map<String, List<com.capgemini.accountserviceapp.adapters.transaction.model.Transaction>> retrieveAndMapAccountTransactions(AccountTransactionsRequest accountTransactionsRequest) {
        Map<String,List<com.capgemini.accountserviceapp.adapters.transaction.model.Transaction>> accountTransactions = new HashMap<>();
        accountTransactionsRequest.getAccountNumbers().forEach(accountNr ->{
            List<Transaction> transactions = this.accountTransactions.with(AccountIdentifier.of(accountNr));
            accountTransactions.put(accountNr,transactions.stream().map(TransactionConverter::convertToTransaction).toList());
        });
        return accountTransactions;
    }


    private void handleCreateTransaction(CreateTransactionRequest createTransactionRequest, Message<Object> message) {
        Transaction transaction = createTransaction(createTransactionRequest);
        try {
            accountTransactions.save(AccountIdentifier.of(createTransactionRequest.getAccountNumber()), transaction);
            CreateAccountTransactionResponse createAccountTransactionResponse = CreateAccountTransactionResponse.builder()
                    .transactionId(transaction.transactionIdentifier().id())
                    .accountNumber(createTransactionRequest.getAccountNumber())
                    .build();
            String transactionResponse = jsonConverter.convertFromObjectToJson(createAccountTransactionResponse);
            message.reply(transactionResponse);
        } catch (RuntimeException exception) {
            logger.error("failed to save transaction for accountId: {}", createTransactionRequest.getAccountNumber());
            message.fail(ErrorCode.CreateAccountException, ErrorCode.CreateAccountExceptionDescription);
        }
    }

    private Transaction createTransaction(CreateTransactionRequest createTransactionRequest) {
        return new Transaction(TransactionIdentifier.of(),
                Instant.now(),
                Amount.of(createTransactionRequest.getAmount(), Currency.valueOf(createTransactionRequest.getCurrency().name())),
                CREATE_ACCOUNT_TRANSACTION_DESC,
                null,
                AccountIdentifier.of(createTransactionRequest.getAccountNumber()));
    }

    private AccountTransactionsResponse convertToAccountTransaction(Map<String,List<com.capgemini.accountserviceapp.adapters.transaction.model.Transaction>> accountTransactions) {
        return AccountTransactionsResponse.builder()
                .accountTransactions(accountTransactions)
                .build();
    }

}