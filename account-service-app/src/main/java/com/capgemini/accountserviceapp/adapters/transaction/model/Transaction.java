package com.capgemini.accountserviceapp.adapters.transaction.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    private String transactionId;
    private String dateTime;
    private long amount;
    private String description;
    private String sourceAccount;
    private String destinationAccount;

}