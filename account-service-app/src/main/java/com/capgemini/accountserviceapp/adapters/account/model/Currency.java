package com.capgemini.accountserviceapp.adapters.account.model;

public enum Currency {
    EUR, GBP, USD
}