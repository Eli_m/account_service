package com.capgemini.accountserviceapp.adapters.account;

import com.capgemini.accountserviceapp.adapters.account.model.CreateAccountRequest;
import com.capgemini.accountserviceapp.adapters.account.model.CustomerAccountsResponse;
import com.capgemini.accountserviceapp.adapters.customer.CustomerService;
import com.capgemini.accountserviceapp.adapters.exception.CustomerNotFoundException;
import com.capgemini.accountserviceapp.adapters.exception.ErrorCode;
import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsRequest;
import com.capgemini.accountserviceapp.adapters.transaction.model.AccountTransactionsResponse;
import com.capgemini.accountserviceapp.adapters.transaction.model.CreateTransactionRequest;
import com.capgemini.accountserviceapp.common.JsonConverter;
import com.capgemini.accountserviceapp.domain.model.AccountIdentifier;
import com.capgemini.accountserviceapp.domain.model.AccountStatus;
import com.capgemini.accountserviceapp.domain.model.Amount;
import com.capgemini.accountserviceapp.domain.model.BankAccount;
import com.capgemini.accountserviceapp.domain.model.Currency;
import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import com.capgemini.accountserviceapp.domain.ports.BankAccounts;
import io.reactivex.rxjava3.core.Observable;
import io.vertx.rxjava3.core.AbstractVerticle;
import io.vertx.rxjava3.core.eventbus.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Component
public class AccountVerticle extends AbstractVerticle {

    private final BankAccounts bankAccounts;
    private final CustomerService customerService;
    private final JsonConverter jsonConverter;

    public AccountVerticle(BankAccounts bankAccounts, CustomerService customerService, JsonConverter jsonConverter) {
        this.bankAccounts = bankAccounts;
        this.customerService = customerService;
        this.jsonConverter = jsonConverter;
    }

    @Override
    public void start() {
        handleCreateAccount();
        handleAccountTransactions();
    }

    private void handleCreateAccount() {
        vertx.eventBus().consumer("com.capgemini.accountservice.account", message -> {
            CreateAccountRequest createAccountRequest = jsonConverter.convertFromJsonToObject(message.body().toString(), CreateAccountRequest.class);
            if (createAccountRequest == null)
                message.fail(ErrorCode.CreateAccountException, ErrorCode.CreateAccountExceptionDescription);
            else {
                processCreateAccount(message, createAccountRequest);
            }
        });

    }

    private void processCreateAccount(Message<Object> message, CreateAccountRequest createAccountRequest) {
        Amount domainAmount = Amount.of(createAccountRequest.getAmount(), Currency.valueOf(createAccountRequest.getCurrency().name()));
        UUID customerUUId = UUIDHelper.fromString(createAccountRequest.getCustomerId());
        Customer customer = customerUUId != null ? customerService.getCustomer(CustomerId.of(customerUUId)) : null;
        if (customer != null)
            createWithCustomerIdAndAmount(customer, domainAmount, message);
        else throw new CustomerNotFoundException(customerUUId);
    }

    private void handleAccountTransactions() {
        vertx.eventBus().consumer("com.capgemini.accountservice.customer.account.transactions", message -> {
            UUID customerUUId = UUIDHelper.fromString(message.body().toString());
            Customer customer = customerService.getCustomer(CustomerId.of(customerUUId));
            if (customer == null) message.reply(null);
            List<BankAccount> bankAccountList = bankAccounts.with(CustomerId.of(customerUUId));

            if (bankAccountList.isEmpty()) {
                assert customer != null;
                message.reply(jsonConverter.convertFromObjectToJson(createCustomerAccountsResponse(null, customer, List.of())));
            } else
                getAccountTransactionsAndMapToResponse(bankAccountList, customer, message);
        });
    }

    private void getAccountTransactionsAndMapToResponse(List<BankAccount> bankAccountList, Customer customer, Message<Object> message) {
        List<String> accountNrs = bankAccountList.stream().map(bankAccount -> bankAccount.accountIdentifier().id()).toList();
        vertx.eventBus().rxRequest("com.capgemini.accountservice.transactions", jsonConverter.convertFromObjectToJson(new AccountTransactionsRequest(accountNrs)))
                .toObservable()
                .flatMap(response -> {
                    AccountTransactionsResponse accountTransactionsResponse = jsonConverter.convertFromJsonToObject(response.body().toString(),AccountTransactionsResponse.class);
                    return Observable.just(jsonConverter.convertFromObjectToJson(createCustomerAccountsResponse(accountTransactionsResponse, customer, bankAccountList)));

                }).subscribe(message::reply, error -> message.reply(error.getMessage()));
    }

    @Transactional
    public void createWithCustomerIdAndAmount(Customer customer, Amount amount, Message<Object> message) {
        BankAccount bankAccount = createBankAccount(customer, amount);
        bankAccounts.save(bankAccount);

        if (!amount.isZero(amount)) {
            CreateTransactionRequest transactionRequest = createTransactionRequest(amount, bankAccount);
            vertx.eventBus().rxRequest("com.capgemini.accountservice.create.transaction", jsonConverter.convertFromObjectToJson(transactionRequest))
                    .subscribe(response -> message.reply(response.body()), error -> message.reply(error.getMessage()));
        }
    }

    private static CreateTransactionRequest createTransactionRequest(Amount amount, BankAccount bankAccount) {
        return CreateTransactionRequest.builder().accountNumber(bankAccount.accountIdentifier().id())
                .amount(amount.amountInCents())
                .currency(com.capgemini.accountserviceapp.adapters.transaction.model.Currency.EUR)
                .build();
    }

    private BankAccount createBankAccount(Customer customer, Amount amount) {
        return new BankAccount(AccountIdentifier.of(),
                customer,
                amount,
                Instant.now(),
                null,
                Instant.now(),
                null,
                AccountStatus.OPEN);
    }

    private CustomerAccountsResponse createCustomerAccountsResponse(AccountTransactionsResponse accountTransactionsResponse, Customer customer, List<BankAccount> bankAccountList) {
        double totalBalance = bankAccountList.stream()
                .mapToLong(bankAccount -> bankAccount.balance().amountInCents())
                .reduce(0, Long::sum);
        return CustomerAccountsResponse.builder()
                .accountTransactions(accountTransactionsResponse)
                .customerName(customer.name())
                .customerSurname(customer.surname())
                .balanceInCent(totalBalance)
                .build();
    }


}

