package com.capgemini.accountserviceapp.adapters.exception;

public class ErrorCode {
    public final static Integer CreateAccountException = 400;
    public final static String CreateAccountExceptionDescription = "failed to save account";
}
