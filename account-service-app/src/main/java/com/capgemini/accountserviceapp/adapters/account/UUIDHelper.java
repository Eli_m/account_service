package com.capgemini.accountserviceapp.adapters.account;

import java.util.UUID;

public class UUIDHelper {
    public static UUID fromString(String uuid){
        try {
            return UUID.fromString(uuid);
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }
}
