package com.capgemini.accountserviceapp.adapters.transaction.model;

public enum Currency {
    EUR, GBP, USD
}
