package com.capgemini.accountserviceapp.adapters.customer;

import com.capgemini.accountserviceapp.domain.model.Customer;
import com.capgemini.accountserviceapp.domain.model.CustomerId;
import com.capgemini.accountserviceapp.domain.ports.Customers;
import org.springframework.stereotype.Service;


//todo: this service should call customer microservice
@Service
public class CustomerService {
    private final Customers customers;

    public CustomerService(Customers customers) {
        this.customers = customers;
    }

    public Customer getCustomer(CustomerId customerId) {
       return customers.with(customerId);
    }

    public void saveCustomer(Customer customer) { customers.save(customer);}
}
