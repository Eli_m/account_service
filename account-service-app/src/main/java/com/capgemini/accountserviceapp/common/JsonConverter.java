package com.capgemini.accountserviceapp.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class JsonConverter {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    public <T> T convertFromJsonToObject(String message, Class<T> clazz) {
        try {
            return objectMapper.readValue(message, clazz);
        } catch (JsonProcessingException exception) {
            return null;
        }
    }

    public String convertFromObjectToJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException exception) {
            return null;
        }
    }
}
